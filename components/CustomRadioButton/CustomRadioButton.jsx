import React from "react";
import classes from "./CustomRadioButton.module.scss";

const CustomRadioButton = () => {
  return (
    <div className={classes.root}>
      <label className={classes.container}>
        1
        <input type="radio" checked="checked" name="radio" />
        <span className={classes.checkmark}></span>
      </label>
      <label className={classes.container}>
        2
        <input type="radio" name="radio" />
        <span className={classes.checkmark}></span>
      </label>
      <label className={classes.container}>
        3
        <input type="radio" name="radio" />
        <span className={classes.checkmark}></span>
      </label>
      <label className={classes.container}>
        4
        <input type="radio" name="radio" />
        <span className={classes.checkmark}></span>
      </label>
    </div>
  );
};

export default CustomRadioButton;
