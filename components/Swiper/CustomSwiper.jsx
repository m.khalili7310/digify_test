import React, { useRef, useState } from "react";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
// Import Swiper styles
import "swiper/css";
import "swiper/css/free-mode";
import "swiper/css/pagination";
import "./CustomSwiper.module.scss";
// import required modules
import { Navigation } from "swiper";
// import required modules
import { FreeMode, Pagination } from "swiper";

export default function CustomSwiper({ children, slidesPerView }) {
  return (
    <>
      <Swiper
        dir="rtl"
        slidesPerView={slidesPerView}
        spaceBetween={20}
        freeMode={true}
        navigation={true}
        // pagination={{
        //   clickable: true,
        // }}
        modules={[FreeMode, Pagination]}
        className="mySwiper"
      >
        {[1, 2, 3, 4, 5, 6, 7].map((item, index) => (
          <>
            <SwiperSlide style={{ margin: "10px 0" }}>{children}</SwiperSlide>
          </>
        ))}
      </Swiper>
    </>
  );
}
