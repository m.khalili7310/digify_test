import React from "react";
import classes from "./MobileDetails.module.scss";
import Rating from "@mui/material/Rating";
import toFarsiNumber from "../../util/toFarsiNumber";
import Typography from "@mui/material/Typography";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import Link from "@mui/material/Link";

const MobileDetails = () => {
  return (
    <div className={classes.root}>
      <div className={classes.rateAndBread}>
        <div className={classes.mobileRate}>
          <Rating name="read-only" value={2} readOnly size="medium" />
          <p>{`(${toFarsiNumber(1250)})`}</p>
        </div>
        <div className={classes.breadcrumbs}>
          <Breadcrumbs aria-label="breadcrumb">
            <Link underline="hover" color="inherit">
              کفش
            </Link>
            <Link underline="hover" color="inherit">
              مردانه
            </Link>
            <Typography color="inherit">محصولات</Typography>
            <Typography color="inherit"> دسته بندی</Typography>
          </Breadcrumbs>
        </div>
      </div>
    </div>
  );
};

export default MobileDetails;
