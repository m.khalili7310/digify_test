import React from "react";
import CustomSwiper from "../Swiper/CustomSwiper";
import ProductCard from "../ProductCard/ProductCard";
import classes from "./SimilarProduct.module.scss";

const SimilarProduct = () => {
  return (
    <div className={classes.root}>
      <h2>محصولات پیشنهادی</h2>
      <CustomSwiper slidesPerView={4.5}>
        <ProductCard />
      </CustomSwiper>
    </div>
  );
};

export default SimilarProduct;
