import { Button } from "@mui/material";
import React from "react";
import classes from "./CustomButton.module.scss";

const primaryColor = {
  backgroundColor: "#555555",
  color: "#ffffff",
  fontFamily: "IRANSansFaNum",
  fontStyle: "normal",
  fontWeight: "400",
  fontSize: "18px",
  lineHeight: "28px",
};

const secondaryColor = {
  backgroundColor: "#E5E5E5",
  color: "#000000",
  fontFamily: "IRANSansFaNum",
  fontStyle: "normal",
  fontWeight: "400",
  fontSize: "18px",
  lineHeight: "28px",
};

const CustomButton = ({ title, color }) => {
  return (
    <div className={classes.root}>
      <Button
        style={color === "secondary" ? secondaryColor : primaryColor}
        variant="contained"
      >
        {title}
      </Button>
    </div>
  );
};

export default CustomButton;
