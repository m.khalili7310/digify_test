import React from "react";
import Image from "next/image";
import classes from "./Comments.module.scss";
import toFarsiNumber from "../../util/toFarsiNumber";

const Comments = () => {
  return (
    <div className={classes.root}>
      <p className={classes.comment}>عالیه من که راضی ام</p>
      <div className={classes.likeAndDate}>
        <p>
          {toFarsiNumber(23)} دی {toFarsiNumber(1400)}{" "}
        </p>
        <Image src="/likeFinger.svg" alt="like" width="22" height="22" />
      </div>
    </div>
  );
};

export default Comments;
