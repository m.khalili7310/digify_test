import React, { useEffect } from "react";
import classes from "./ProductPreview.module.scss";
import ProductImages from "../../components/ProductImages/ProductImages";
import ProductDetails from "../../components/ProductDetails/ProductDetails";
import productActions from "../../store/product/product.actions";
import MobileDetails from "../../components/MobileDetails/MobileDetails";

const ProductPreview = ({ data }) => {
  useEffect(() => {
    productActions.selectedProduct({ payload: data });
  }, []);

  return (
    <div className={classes.root}>
      <ProductImages />
      <ProductDetails />
      <MobileDetails />
    </div>
  );
};

export default ProductPreview;
