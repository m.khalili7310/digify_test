import React from "react";
import Image from "next/image";
import classes from "./MobileMenu.module.scss";

const MobileMenu = () => {
  return (
    <div className={classes.root}>
      <div className={classes.left}>
        <span className={classes.imageLeft}>
          <Image src="/menuIcon.svg" height={24} width={24} alt="search" />
        </span>
        <span className={classes.imageLeft}>
          <Image
            className={classes.imageLeft}
            src="/shareIcon.svg"
            height={24}
            width={24}
            alt="search"
          />
        </span>
        <span className={classes.imageLeft}>
          <Image
            className={classes.imageLeft}
            src="/likeIcon.svg"
            height={24}
            width={24}
            alt="search"
          />
        </span>
      </div>
      <div className={classes.right}>
        <Image src="/arrowRightIcon.svg" height={24} width={24} alt="search" />
      </div>
    </div>
  );
};

export default MobileMenu;
