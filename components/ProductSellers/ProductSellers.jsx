import Image from "next/image";
import React from "react";
import classes from "./ProductSellers.module.scss";
import Rating from "@mui/material/Rating";
import Typography from "@mui/material/Typography";
import CustomButton from "../CustomButton/CustomButton";
import toFarsiNumber from "../../util/toFarsiNumber";
import PersianPrice from "../PersianPrice/PersianPrice";

const ProductSellers = () => {
  return (
    <div className={classes.root}>
      <h2>فروشندگان این محصول</h2>
      {[1, 2, 3, 4, 5].map((seller, index) => (
        <div key={index} className={classes.sellerContainer}>
          <div className={classes.icons}>
            <span>
              <Image
                src="/marketIcon.svg"
                alt="market"
                width="15"
                height="15"
              />{" "}
              کفش شیک
            </span>
            <span>
              <Image
                src="/certificateIcon.svg"
                alt="market"
                width="15"
                height="15"
              />{" "}
              {toFarsiNumber(6)} ماه ضمانت کالا
            </span>
          </div>
          <div className={classes.price}>
            <PersianPrice number={849000} />{" "}
            <span className={classes.toman}>تومان</span>
          </div>
          <div className={classes.rate}>
            <p>{toFarsiNumber(4.6)}</p>
            <Rating name="read-only" value={2} readOnly size="small" max={1} />
          </div>
          <div>
            <CustomButton title="همین حالا بخرید" color="primary" />
          </div>
        </div>
      ))}{" "}
    </div>
  );
};

export default ProductSellers;
