import Image from "next/image";
import React from "react";
import PersianPrice from "../PersianPrice/PersianPrice";
import classes from "./ProductCard.module.scss";

const ProductCard = () => {
  return (
    <div className={classes.root}>
      <Image
        src="/product/1/eeb4a70c24827792816e0c309d21d34330ffea28_1607358181.png"
        alt="pic"
        width="100%"
        height="100%"
        layout="responsive"
        objectFit="contain"
      />
      <hr />
      <p>کفش اسپرت مردانه</p>
      <p>
        <PersianPrice number={820000} /> تومان
      </p>
    </div>
  );
};

export default ProductCard;
