import React from "react";
import classes from "./TopNav.module.scss";
import Box from "@mui/material/Box";
import Image from "next/image";
import Menu from "../Menu/Menu";
import MobileMenu from "../MobileMenu/MobileMenu";

const TopNav = () => {
  return (
    <div className={classes.root}>
      <div className={classes.headerSearch}>
        <Image src="/search.svg" height={24} width={24} alt="search" />
      </div>
      <div className={classes.headerMenu}>
        <Menu />
      </div>
      <div className={classes.headerTheme}>
        <Image src="/themeIcon.svg" height={24} width={24} alt="search" />
      </div>
      <MobileMenu className={classes.mobileMenu} />
    </div>
  );
};

export default TopNav;
