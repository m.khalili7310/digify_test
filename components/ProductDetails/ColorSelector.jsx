import React from "react";
import classes from "./ColorSelector.module.scss";

const ColorSelector = ({ product }) => {
  const { colors } = product[0];
  return (
    <div className={classes.root}>
      <h2>انتخاب رنگ</h2>
      <ul className={classes.DetailsUl}>
        {colors.map((color, index) => (
          <li
            key={index}
            style={{ background: color }}
            className={`${classes.DetailsLi} ${
              index === 3 && classes.DetailsLiSelected
            }`}
          ></li>
        ))}
      </ul>
    </div>
  );
};

export default ColorSelector;
