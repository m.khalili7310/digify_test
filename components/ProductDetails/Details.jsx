import Image from "next/image";
import React from "react";
import classes from "./Details.module.scss";

const Details = ({ product }) => {
  const { description } = product[0];
  return (
    <div className={classes.root}>
      <h2>مشخصات</h2>
      <ul className={classes.DetailsUl}>
        {description.map((desc, index) => (
          <li key={index} className={classes.DetailsLi}>
            <span className={classes.liDot}>
              <Image
                src="/bullets.svg"
                alt="bullets"
                width="8px"
                height="8px"
              />
            </span>
            <span className={classes.titleLi}>{desc.title}:</span>{" "}
            <span className={classes.descriptionsLi}>{desc.desc} </span>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Details;
