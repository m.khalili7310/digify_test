import React from "react";
import Details from "./Details";
import MainDetails from "./MainDetails";
import SizeSelector from "./SizeSelector";
import classes from "./ProductDetails.module.scss";
import ColorSelector from "./ColorSelector";
import { useSelector } from "react-redux";

const ProductDetails = () => {
  const product = useSelector((state) => state.product);

  return (
    <div className={classes.root}>
      {product.length > 0 && (
        <>
          <MainDetails product={product} />
          <hr />
          <Details product={product} />
          <hr />
          <SizeSelector product={product} />
          <hr />
          <ColorSelector product={product} />
        </>
      )}
    </div>
  );
};

export default ProductDetails;
