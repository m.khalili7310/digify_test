import React from "react";
import classes from "./SizeSelector.module.scss";
import toFarsiNumber from "../../util/toFarsiNumber";

const SizeSelector = ({ product }) => {
  const { sizes } = product[0];
  return (
    <div className={classes.root}>
      <h2>انتخاب سایز</h2>
      <ul className={classes.DetailsUl}>
        {sizes.map((size, index) => (
          <li
            key={index}
            className={`${classes.DetailsLi} ${
              index === 2 && classes.DetailsLiSelected
            }`}
          >
            {toFarsiNumber(size)}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default SizeSelector;
