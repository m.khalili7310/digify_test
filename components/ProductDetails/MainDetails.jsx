import React from "react";
import CustomButton from "../CustomButton/CustomButton";
import toFarsiNumber from "../../util/toFarsiNumber";
import classes from "./MainDetails.module.scss";
import PersianPrice from "../PersianPrice/PersianPrice";

const MainDetails = ({ product }) => {
  const { title, code, price, off } = product[0];

  const calcPercent = (price, off) => {
    const percent = price / 100;
    const val = off * percent;
    return price - val;
  };

  return (
    <div className={classes.root}>
      <h1>
        {title} کد {toFarsiNumber(code)}
      </h1>
      <div className={classes.priceContainer}>
        <div className={classes.price}>
          <PersianPrice number={calcPercent(price, off)} /> تومان
        </div>
        <div className={classes.offPrice}>
          <span className={classes.offPercent}>{toFarsiNumber(off)}%</span>
          <span className={classes.lastPrice}>
            <PersianPrice number={price} />
          </span>
        </div>
      </div>
      <div className={classes.buttonContainer}>
        <CustomButton title="همین حالا بخرید" color="primary" />
        <CustomButton title="افزودن به سبد خرید" color="secondary" />
      </div>
    </div>
  );
};

export default MainDetails;
