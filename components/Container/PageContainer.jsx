import React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import classes from "./PageContainer.module.scss";
import TopNav from "../TopNav/TopNav";

const PageContainer = ({ children }) => {
  return (
    <div className={classes.root}>
      <CssBaseline />
      <Container>
        <TopNav />
        {children}
      </Container>
    </div>
  );
};

export default PageContainer;
