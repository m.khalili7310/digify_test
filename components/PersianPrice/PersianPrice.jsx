import PropTypes from "prop-types";
import React, { Component } from "react";

class PersianPrice extends Component {
  render() {
    let en_number = this.props.number;
    const persian_number = new Intl.NumberFormat("fa").format(en_number);
    return <span>{persian_number}</span>;
  }
}

PersianPrice.propTypes = {
  number: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
};
export default PersianPrice;
