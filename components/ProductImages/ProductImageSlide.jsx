import Image from "next/image";
import React from "react";
import classes from "./ProductImageSlide.module.scss";

const ProductImageSlide = ({ product }) => {
  const { image } = product[0];

  return (
    <div className={classes.root}>
      {image.map((item, index) => (
        <div key={index} className={classes.slideContainer}>
          <Image src={`${item}.png`} alt="image" width="90" height="90" />
        </div>
      ))}
    </div>
  );
};

export default ProductImageSlide;
