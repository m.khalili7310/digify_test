import React from "react";
import ImageAndRate from "./ImageAndRate";
import classes from "./ProductImages.module.scss";
import ProductImageSlide from "./ProductImageSlide";
import { useSelector } from "react-redux";

const ProductImages = () => {
  const product = useSelector((state) => state.product);
  return (
    <div className={classes.root}>
      {product.length > 0 && (
        <>
          <ImageAndRate product={product} />
          <ProductImageSlide product={product} />
        </>
      )}
    </div>
  );
};

export default ProductImages;
