import Image from "next/image";
import React from "react";
import classes from "./ImageAndRate.module.scss";
import Rating from "@mui/material/Rating";
import PercentIcon from "@mui/icons-material/Percent";
import CustomRadioButton from "../CustomRadioButton/CustomRadioButton";
import toFarsiNumber from "../../util/toFarsiNumber";

const ImageAndRate = ({ product }) => {
  const { image } = product[0];
  return (
    <div className={classes.root}>
      <div className={classes.mainImage}>
        <div className={classes.mainHeader}>
          <div>
            <CustomRadioButton />
          </div>
          <span className={classes.percent}>
            <PercentIcon />
          </span>
        </div>
        <div className={classes.imageContainer}>
          <Image
            src={`${image[0]}.png`}
            alt="image"
            width="100%"
            height="100%"
            layout="responsive"
            objectFit="contain"
          />
        </div>
      </div>
      <div className={classes.rateAndLike}>
        <div className={classes.like}>
          <Image src="/shareIcon.svg" alt="share" width="20" height="20" />
          <Image src="/likeIcon.svg" alt="like" width="20" height="20" />
        </div>
        <div className={classes.rate}>
          <Rating name="read-only" value={2} readOnly size="medium" />
          <p>{`(${toFarsiNumber(1250)})`}</p>
        </div>
      </div>
    </div>
  );
};

export default ImageAndRate;
