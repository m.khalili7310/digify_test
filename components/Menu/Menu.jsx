import Link from "next/link";
import React from "react";
import classes from "./Menu.module.scss";

const Menu = () => {
  return (
    <nav className={classes.root}>
      <ul className={classes.menu}>
        <Link href="/products">
          <a>
            <li className={classes.item}>صفحه اصلی</li>
          </a>
        </Link>
        <Link href="/products">
          <a>
            <li className={`${classes.item} ${classes.itemSelected}`}>
              محصولات
            </li>
          </a>
        </Link>
        <Link href="/products">
          <a>
            <li className={classes.item}>خدمات</li>
          </a>
        </Link>
        <Link href="/products">
          <a>
            <li className={classes.item}>وبلاگ</li>
          </a>
        </Link>
        <Link href="/products">
          <a>
            <li className={classes.item}>درباره ما</li>
          </a>
        </Link>
      </ul>
    </nav>
  );
};

export default Menu;
