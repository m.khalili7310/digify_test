import React from "react";
import CustomSwiper from "../Swiper/CustomSwiper";
import Comment from "../Comments/Comments";
import classes from "./UserComments.module.scss";

const UserComments = () => {
  return (
    <div className={classes.root}>
      <h2>نظرات کاربران</h2>
      <CustomSwiper slidesPerView={5.5}>
        <Comment />
      </CustomSwiper>
    </div>
  );
};

export default UserComments;
