import React, { useEffect } from "react";
import classes from "./Product.module.scss";
import ProductPreview from "../../components/ProductPreview/ProductPreview";
import ProductSellers from "../../components/ProductSellers/ProductSellers";
import SimilarProduct from "../../components/SimilarProduct/SimilarProduct";
import UserComments from "../../components/UserComments/UserComments";

const index = ({ data }) => {
  return (
    <>
      <ProductPreview data={data} />
      <hr style={{ borderTop: "1px solid #eaeaea", width: "92%" }} />
      <ProductSellers />
      <hr
        style={{
          borderTop: "1px solid #eaeaea",
          width: "92%",
          marginTop: "20px",
        }}
      />
      <SimilarProduct />
      <hr
        style={{
          borderTop: "1px solid #eaeaea",
          width: "92%",
          marginTop: "20px",
        }}
      />
      <UserComments />
    </>
  );
};

export async function getServerSideProps() {
  // Fetch data from external API
  const res = await fetch(`http://localhost:8000/product`);
  const data = await res.json();

  // Pass data to the page via props
  return { props: { data } };
}

export default index;
