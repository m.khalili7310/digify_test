import { Provider } from "react-redux";
import PageContainer from "../components/Container/PageContainer";
import store from "../store";
import "../styles/globals.css";

function MyApp({ Component, pageProps }) {
  return (
    <Provider store={store}>
      <PageContainer>
        <Component {...pageProps} />
      </PageContainer>
    </Provider>
  );
}

export default MyApp;
