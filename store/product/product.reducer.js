function productReducer(state = [], action) {
  switch (action.type) {
    case "selected-product":
      return action.payload;
    default:
      return state;
  }
}

export default productReducer;
