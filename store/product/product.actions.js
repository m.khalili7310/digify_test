import { bindActionCreators } from "redux";
import store from "..";

function selectedProduct({ payload }) {
  return {
    type: "selected-product",
    payload: payload,
  };
}

const productActions = bindActionCreators({ selectedProduct }, store.dispatch);

export default productActions;
